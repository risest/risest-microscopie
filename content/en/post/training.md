---
date: 2022-09-01
description: ""
featured_image: "/images/pie-chart.png"
tags: []
title: "Training Online"
---
Microscopy is not just a beautiful image. RISE offers you the opportunity to develop your knowledge in microscopy, image analysis and programming.

---
date: 2022-09-01
description: "RISE offers you a wide range of microscopes ranging from wide-field microscopes to high resolution, including confocal microscopy."
featured_image: "/images/microscope.png"
tags: []
title: "Light Microscopy"
---

# Fixed sample
The principle is based on the observation of your samples after fixation.

# Live imaging
Study cellular structure, function and organization in your living cells using real-time imaging.

# Blade Scanner
Faster more volume!

# Microdissector
Fire!!!

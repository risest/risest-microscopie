---
title: "About"
description: "Réseau Imagerie Strasbourg et grand Est: RISEst is a network that brings together 6 imaging platforms and 25 research units across 9 institutes."
featured_image: '/images/strasbourg.jpg'
menu:
  main:
    weight: 1
---
{{< figure src="/images/home-carte.png" title="" >}}

Réseau Imagerie Strasbourg et grand Est: RISEst is a network that brings together 6 imaging platforms and 25 research units across 9 institutes.

- Centre de recherche en biomédecine de Strasbourg [(CRBS)](https://crbs.unistra.fr/)
- Institut de biologie moléculaire des plantes [(IBMP)](http://www.ibmp.cnrs.fr/)
- Institu de gènétique et de biologie moléculaire et cellulaire [(IGBMC)](https://www.igbmc.fr/)
- Laboratoire de bioimagerie et pathologies [(LBP)](https://lbp.unistra.fr/)
- Institut de Biologie Moléculaire et Cellulaire [(IBMC)](https://ibmc.cnrs.fr/)
- Institut des Neurosciences Cellulaires et Intégratives [(INCI)](https://inci.u-strasbg.fr/)
- Laboratoire de neurosciences cognitives et adaptatives [(LNCA)](https://www.lnca.cnrs.fr/)
- Institut de Recherche en Informatique, Mathématiques, Automatique et Signal [(IRIMAS)](https://www.irimas.uha.fr/)


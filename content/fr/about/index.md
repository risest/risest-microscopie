---
title: "About"
description: "Réseau Imagerie Strasbourg et grand Est: RISEst est un réseau qui regroupe 6 plateformes d'imagerie et 25 unités de recherche réparties dans 9 instituts."
featured_image: '/images/strasbourg.jpg'
menu:
  main:
    weight: 1
---

![](/images/home-carte.png)

RISEst est un réseau qui regroupe 6 plateformes d'imagerie et 25 unités de recherche réparties dans 9 instituts.

- Centre de recherche en biomédecine de Strasbourg [(CRBS)](https://crbs.unistra.fr/)
- Institut de biologie moléculaire des plantes [(IBMP)](http://www.ibmp.cnrs.fr/)
- Institu de gènétique et de biologie moléculaire et cellulaire [(IGBMC)](https://www.igbmc.fr/)
- Laboratoire de bioimagerie et pathologies [(LBP)](https://lbp.unistra.fr/)
- Institut de Biologie Moléculaire et Cellulaire [(IBMC)](https://ibmc.cnrs.fr/)
- Institut des Neurosciences Cellulaires et Intégratives [(INCI)](https://inci.u-strasbg.fr/)
- Laboratoire de neurosciences cognitives et adaptatives [(LNCA)](https://www.lnca.cnrs.fr/)
- Institut de Recherche en Informatique, Mathématiques, Automatique et Signal [(IRIMAS)](https://www.irimas.uha.fr/)


---
date: 2022-09-01
description: ""
featured_image: "/images/pie-chart.png"
tags: []
title: "Formation Online"
---
La microscopie, ce  n'est pas qu'une belle image. RISE vous  offre l'opportunité de développer vos connaissances en microscopie, analyse d'image et programmation

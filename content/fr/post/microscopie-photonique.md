---
date: 2022-09-01
description: "RISE vous propose une large gamme de microscope allant du microscope grand champ à la haute résolution en passant par la microscopie confocale"
featured_image: "/images/microscope.png"
tags: []
title: "Microscopie Photonique"
---

# Fixe sample
Le principe repose sur l'observation de vos échantillons après fixation

# Live imaging
Etudier la structure, le fonctionnement et l'organisation cellulaires dans vos cellules vivantes grâce à l'imagerie en temps réelle.

# Scanner de lame
Plus vite plus de volume

# Microdissecteur
Feu!!!

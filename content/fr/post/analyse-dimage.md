---
date: 2022-09-01
description: ""
featured_image: "/images/image-stack.png"
tags: []
title: "Analyse d'image"
---
D'un simple comptage à un pipeline de deep leaning complexe , Rise peut vous conseiller et vous orienter dans vos choix afin d'extraire les données de vos images.
